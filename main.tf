provider "aws" {
  profile = "default"
  region  = var.aws_region
}
resource "aws_vpc" "srv_vpc" {
  cidr_block = "${var.vpc_cidr_blk}"

  tags = {
    Name = "srv_vpc"
  }
}

resource "aws_subnet" "srv_subnet" {
  vpc_id            = aws_vpc.srv_vpc.id
  cidr_block        = "${var.vpc_cidr_blk}"
  availability_zone = "me-south-1a"

  tags = {
    Name = "vpc_subnet"
  }
}

resource "aws_network_interface" "deb_ni" {
  subnet_id  = aws_subnet.srv_subnet.id
  private_ip = "192.168.0.100"

  tags = {
    Name = "deb_primary_ni"
  }
}

resource "aws_instance" "deb" {
  ami           = "ami-08ca9d63b8d4ec276"
  instance_type = "t3.micro"

  network_interface {
    network_interface_id = aws_network_interface.deb_ni.id
    device_index         = 0
  }
  tags = {
    Name = "DEB_SRV"
  }
}
resource "aws_network_interface" "rh_ni" {
  subnet_id  = aws_subnet.srv_subnet.id
  private_ip = "192.168.0.200"

  tags = {
    Name = "rh_primary_ni"
  }
}

resource "aws_instance" "rh" {
  ami           = "ami-05c34053888834402"
  instance_type = "t3.micro"

  network_interface {
    network_interface_id = aws_network_interface.rh_ni.id
    device_index         = 0
  }
  tags = {
    Name = "RH_SRV"
  }
}
