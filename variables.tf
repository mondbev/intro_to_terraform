variable "aws_region" {
  default = "me-south-1"
  type = string
  description = "Default region for AWS"
}

variable "vpc_cidr_blk" {
  default = "192.168.0.0/24"
  type = string
  description = "Default CIDR block for AWS VPC"
}
